//
//  MapViewController.swift
//  Location
//
//  Created by Чернецова Юлия on 15/09/2019.
//  Copyright © 2019 Чернецов Роман. All rights reserved.
//

import UIKit
import Foundation
import GoogleMaps
import MapKit
import CoreLocation
import RealmSwift

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager: CLLocationManager?
    var geocoder = CLGeocoder()
    var marker: GMSMarker!
    let coordinate = CLLocationCoordinate2D(latitude: 59.939095, longitude: 30.315868)
    //let path = GMSMutablePath()
    static var realm = try! Realm(configuration: Realm.Configuration(deleteRealmIfMigrationNeeded: true))
    var route: GMSPolyline?
    var routePath: GMSMutablePath?
    var storeCoordinates = [Coordinates]()
    var timer: Timer?
    var isTracking = false
    var identificatorTask: UIBackgroundTaskIdentifier?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMap() 
        configureLocationManager()
        addMarker(position: coordinate)
        identificatorTask = UIApplication.shared.beginBackgroundTask{ [weak self] in
            guard let self = self else {return}
            UIApplication.shared.endBackgroundTask(self.identificatorTask!)
            self.identificatorTask = UIBackgroundTaskIdentifier.invalid
        }
        NotificationCenter.default.addObserver(
            forName: UIApplication.didEnterBackgroundNotification,
            object: nil,
            queue: OperationQueue.main) { [weak self] _ in
                UserDefaults.standard.set(self?.isTracking, forKey: "Tracking")
        }
        NotificationCenter.default.addObserver(
            forName: UIApplication.willEnterForegroundNotification,
            object: nil, queue: OperationQueue.main) { [weak self] _ in
                self?.isTracking  = UserDefaults.standard.bool(forKey: "Tracking")
                print("\(String(describing: self?.isTracking))")
        }
    }
    
    func saveData() {
        guard let objects = RealmProvider.get(Coordinates.self)?.toArray(type: Coordinates.self) else {return}
        RealmProvider.delete(objects)
        if (storeCoordinates.count>0) {
            RealmProvider.save(items: storeCoordinates)
            //storeCoordinates = [Coordinates]()
        }
    }
    
    func configureLocationManager(){
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.distanceFilter = 200
        self.locationManager?.requestAlwaysAuthorization()
        self.locationManager?.requestWhenInUseAuthorization()
        self.locationManager?.allowsBackgroundLocationUpdates = true
        self.locationManager?.startUpdatingLocation()
        self.isTracking = true
        configureRoute(objects: storeCoordinates)
    }
    func configureMap(){
        let camera = GMSCameraPosition.camera(withTarget: coordinate, zoom:17)
        mapView.camera = camera
        mapView.delegate = self
    }
    
    func configureRoute(objects:[Coordinates]){
        self.route = GMSPolyline()
        self.routePath = GMSMutablePath()
        self.route?.strokeWidth = 5
        self.route?.strokeColor = .green
        self.route?.map = mapView
        if objects.count>0 {
            for current in objects {
                self.routePath?.add(CLLocationCoordinate2DMake(current.latitude, current.longitude))
            }
            self.route?.path = self.routePath!
            let bounds = GMSCoordinateBounds(coordinate: CLLocationCoordinate2DMake(objects.first!.latitude, objects.first!.longitude), coordinate: CLLocationCoordinate2DMake(objects.last!.latitude, objects.last!.longitude))
            let camera = mapView.camera(for: bounds, insets: UIEdgeInsets())!
            mapView.camera = camera
        }
    }
    
    @IBAction func currenPosition(_ sender: Any) {
        locationManager?.requestLocation()
    }
    
    @IBAction func previusTrack(_ sender: Any) {
        self.route?.map = nil
        if isTracking {
            UIAlertController.showAlert(title: "Статус слежения", message: "Необходимо выключить слежение! \n Отключить?", inViewController: self) {
                self.locationManager?.stopUpdatingLocation()
                self.isTracking = false
                self.saveData()
                self.configureRoute(objects: self.storeCoordinates)
            }

        } else {self.configureRoute(objects: self.storeCoordinates)}
   
    }
    @IBAction func updateLocation(_ sender: Any) {
        // Заменяем старую линию новой
        route?.map = nil
        // Заменяем старую линию новой
        route = GMSPolyline()
        // Заменяем старый путь новым, пока пустым (без точек)
        routePath = GMSMutablePath()
        route?.strokeWidth = 5
        route?.strokeColor = .green
        // Добавляем новую линию на карту
        route?.map = mapView
        locationManager?.startUpdatingLocation()
        isTracking = true
        storeCoordinates = [Coordinates]()
        locationManager?.pausesLocationUpdatesAutomatically = false
       
    }
    
    @IBAction func stopUpdateLacation(_ sender: Any) {
        if isTracking {
            locationManager?.stopUpdatingLocation()
            saveData()
            isTracking = false
        }
        // Отвязываем от карты старую линию
        route?.map = nil
    }
    
    func addMarker(position: CLLocationCoordinate2D) {
        let marker = GMSMarker(position: position)
        marker.icon = GMSMarker.markerImage(with: .green)
        marker.title = "Маркет"
        marker.snippet = "овощной"
        marker.map = mapView
        self.marker = marker
    }
}

extension MapViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        addMarker(position: coordinate)
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker
        let point = mapView.projection.point(for: marker.position)
        let camera = mapView.projection.coordinate(for: point)
        let position = GMSCameraUpdate.setTarget(camera)
        mapView.animate(with: position)
        return true
    }
}
extension MapViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        storeCoordinates.append(Coordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        self.routePath?.add(location.coordinate)
        self.route?.path = self.routePath!
        let position = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 13)
        self.mapView.animate(to: position)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}
//        if startCoordinate.longitude == 0.1{
//            startCoordinate.longitude = location.coordinate.longitude
//            startCoordinate.latitude = location.coordinate.latitude
//        }
//        let center = location.coordinate
//        let bounds = GMSCoordinateBounds(coordinate: center, coordinate: startCoordinate)
//        mapView.camera(for: bounds, insets: UIEdgeInsets())
//let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//        print("количество \(String(describing: routePath?.count()))")
//        print("широта: \(location.coordinate.latitude)  и долгота: \(location.coordinate.longitude)")
//        geocoder.reverseGeocodeLocation(location) { (places, error) in
//           print(places?.first as Any)
//        }
//        let position = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 13)
//        self.mapView.animate(to: position)
//        let vancouver = CLLocationCoordinate2D(latitude: 49.26, longitude: -123.11)
//        let calgary = CLLocationCoordinate2D(latitude: 51.05,longitude: -114.05)
//        let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
//        let camera = mapView.camera(for: bounds, insets: UIEdgeInsets())!
//      mapView.camera = camera
//    func fetchRoute(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D){
//        let session = URLSession.shared
//        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(from.longitude),\(from.latitude)&destination=\(to.longitude),\(to.latitude)&mode=driving&key=AIzaSyBFgtXJGEQMBj16k3EEdFTYmny5_qg9v_0")
//
////       https://maps.googleapis.com/maps/api/directions/json?origin=\(39.854811),\(57.633913)&destination=\(39.856272),\(57/.636167)&mode=driving&key=AIzaSyBFgtXJGEQMBj16k3EEdFTYmny5_qg9v_0
//        let task = session.dataTask(with: url!) {
//            (data,result,error) in
//            guard error == nil else {return print(error as Any)}
//            guard let json = try? JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) else {return}
//            print(json)
//        }
//        task.resume()
//    }
//   mapView.animate(toLocation: coordinate)
//   fetchRoute(from: CLLocationCoordinate2D(latitude: 57.633913, longitude: 39.854811), to: CLLocationCoordinate2D(latitude: 57.636167, longitude: 39.856272))
//func drawPath(path:GMSMutablePath) {
//    //let path = GMSPath(fromEncodedPath: path)
//    let polyline = GMSPolyline(path: path)
//    polyline.strokeWidth = 5
//    polyline.strokeColor = .green
//    polyline.map = mapView
//}
   //Realm.Configuration.defaultConfiguration.fileURL!
//            let position = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2DMake(objects.last!.latitude,objects.last!.longitude), zoom: 13)
//            self.mapView.animate(to: position)
