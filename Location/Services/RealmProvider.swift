//
//  RealmProvider.swift
//  Location
//
//  Created by Чернецова Юлия on 29/09/2019.
//  Copyright © 2019 Чернецов Роман. All rights reserved.
//

import Foundation
import RealmSwift

class RealmProvider{
    static func save<T:Object>(items: [T], config: Realm.Configuration = Realm.Configuration(deleteRealmIfMigrationNeeded: true), update: Bool = true){
        do{
            let realm = try Realm(configuration: config)
            try realm.write {
                realm.add(items, update: update)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
  
    static func get<T:Object>(_ type: T.Type, config: Realm.Configuration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)) ->Results<T>?{
        do{
            let realm = try Realm(configuration: Realm.Configuration(deleteRealmIfMigrationNeeded: true))
            return realm.objects(type)
        }
        catch {
            print(error)
        }
        return nil
    }
    
    static func delete<T: Object>(_ items: [T],
                                  config: Realm.Configuration = Realm.Configuration.defaultConfiguration) {
        
        do{
            let realm = try Realm(configuration: Realm.Configuration.defaultConfiguration)
            
            try? realm.write {
                realm.delete(items)
            }
        }
            
        catch {
            print(error)
        }
        
    }
    
//    static func saveCoordinates(_ coordinates: [Coordinates],
//                                 id: Int,
//                                 config: Realm.Configuration = Realm.Configuration(deleteRealmIfMigrationNeeded: true)) {
//        do {
//            let realm = try Realm(configuration: config)
//            guard let user = realm.object(ofType: coordinates.self, forPrimaryKey: id) else { return }
//            try realm.write {
//                user.photos.append(objectsIn: photos)
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//    }
    //    static func saveNewsForUser( _ news:[News], id:Int, config: Realm.Configuration = Realm.Configuration(deleteRealmIfMigrationNeeded: true))
    //    {
    //        do {
    //            let realm = try Realm(configuration: config)
    //            guard let user = realm.object(ofType: User.self, forPrimaryKey: id) else { return }
    //            try realm.write {
    //                user.news.append(objectsIn: news)
    //            }
    //        } catch {
    //            print(error.localizedDescription)
    //        }
    //    }
    //
    //    static func  saveNewsForGroup( _ news:[News], id:Int, config: Realm.Configuration = Realm.Configuration(deleteRealmIfMigrationNeeded: true))
    //    {
    //        do {
    //            let realm = try Realm(configuration: config)
    //            guard let group = realm.object(ofType: Group.self, forPrimaryKey: id) else { return }
    //            try realm.write {
    //                group.news.append(objectsIn: news)
    //            }
    //        } catch {
    //            print(error.localizedDescription)
    //        }
    //    }
   // Realm.Configuration = Realm.Configuration.defaultConfiguration
}
extension IndexPath {
    static func fromRow(_ row: Int) -> IndexPath {
        return IndexPath(row: row, section: 0)
    }
}
extension UITableView {
    func applyChanges(deletions: [Int], insertions: [Int], updates: [Int]) {
        beginUpdates()
        deleteRows(at: deletions.map(IndexPath.fromRow), with: .automatic)
        insertRows(at: insertions.map(IndexPath.fromRow), with: .automatic)
        reloadRows(at: updates.map(IndexPath.fromRow), with: .automatic)
        endUpdates()
    }
}


extension Results {
    func toArray<T>(type: T.Type) -> [T] {
        return compactMap { $0 as? T }
    }
}
