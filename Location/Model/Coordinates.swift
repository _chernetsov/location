//
//  Coordinates.swift
//  Location
//
//  Created by Чернецова Юлия on 28/09/2019.
//  Copyright © 2019 Чернецов Роман. All rights reserved.
//

import Foundation
import RealmSwift
class Coordinates:Object{
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    convenience  init(latitude: Double, longitude: Double) {
        self.init()
        self.latitude = latitude
        self.longitude = longitude
    }
    
    override static func primaryKey()->String {
        return "id"
    }
}
